name := "repl-with-libraries"

version := "0.1"

libraryDependencies ++= Seq(
  "com.google.guava" % "guava" % "18.0",
  "org.apache.commons" % "commons-lang3" % "3.3.2",
  "org.jsoup" % "jsoup" % "1.8.1"
)